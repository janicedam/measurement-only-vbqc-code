import os
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import yaml
from math import pi
from argparse import ArgumentParser
from yaml.loader import SafeLoader

font = {'size': 12}

matplotlib.rc('font', **font)

NAME2DISPLAY = { "coherence_time": "coherence\n time",
                "emission_fidelity": "emission\n fidelity",
                "p_loss_init": "server\n efficiency",
                "single_qubit_depolar_prob": "single qubit\n gate fidelity",
                "ms_depolar_prob": "entangling\n gate fidelity"}

TO_PROB_NO_ERROR_FUNCTION = {"p_loss_init": lambda x: 1.0 - x,
                             "single_qubit_depolar_prob": lambda x: 1.0 - x,
                             "ms_depolar_prob": lambda x: 1.0 - x,
                             "coherence_time": lambda x: np.exp(-1.0 * (10000.0 / x)**2),
                             "emission_fidelity": lambda x: (4 * x - 1) / 3
                             }


def plot_circular(improvement_dictionary, max_radius=None, baseline_name="baseline",
                  solution_name="improved", path="circular_plot", log=False):
    angles = [n / float(len(improvement_dictionary)) * 2 * pi for n in range(len(improvement_dictionary))]
    # Change the radial values by adding 1 to each value
    r = [value + 1 for value in improvement_dictionary.values()]

    ax = plt.subplot(111, polar=True)
    ax.set_axisbelow(False)
    ax.plot(angles + angles[0:], r + r[0:], marker='o', linestyle=':')#'o', linewidth=2.5)

    if max_radius is not None:
        ax.set_rmax(max_radius + 1)
        if max_radius > 10:
            ticks = [round(tick / 5) * 5 for tick in np.arange(0, max_radius, max_radius / 4)]
            ax.set_rticks(ticks)
        else:
            ax.set_rticks(np.arange(1, max_radius, 2))
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)
    plt.xticks(angles)
    angle_labels = [NAME2DISPLAY[param_name] for param_name in improvement_dictionary.keys()]
    ax.set_xticklabels(angle_labels)
    ax.xaxis.set_tick_params(pad=22)
    ax.set_rlabel_position(0)
    ax.grid(True)
    if log:
        ax.set_rscale('symlog')
    ax.text(0.83, ax.get_rmax()-1.4, 'Imp. factor',
            rotation=0, ha='center', va='center', size=12)
    
    # Add point labels near the plotted points
    for angle, radius, label in zip(angles, [x+0.7 for x in r], [round(x, 1) for x in r]):
        x, y = angle, radius
        plt.text(x, y, label, ha="center", va="center", color='royalblue')

    # Add an arrow coming from the center and pointing towards 'Imp. factor'
    ax.annotate('', xy=(0.5, 0), xytext=(0.5, ax.get_rmax() - 0.3),
                arrowprops=dict(arrowstyle='<-'))
    
    # Connect points with dotted lines
    #ax.plot(angles + angles[0:], r + r[0:], linestyle='dotted', color='royalblue', label='Dotted Lines')

    # Saves
    filename = 'circular_plot_baseline_{}_improved_{}'.format(baseline_name, solution_name)
    my_path = os.path.abspath(".")
    path_to_save = os.path.join(my_path, path)
    if not os.path.exists(path_to_save):
        os.mkdir(path_to_save)
    # plt.show()
    plt.savefig(os.path.join(path_to_save, filename + '.pdf'), dpi=300, bbox_inches='tight')
    plt.savefig(os.path.join(path_to_save, filename + '.png'), dpi=300, bbox_inches='tight')


def compute_improvement_factor_dict(baseline_dict, solution_dict):
    improvement_factor_dict = {}

    for parameter, value in baseline_dict.items():
        # Some TI parameters are passed as improvement factors, hence they can just directly be added to the cost
        if "improvement" in parameter:
            improvement_factor_dict[parameter] = solution_dict[parameter]
            continue
        baseline_prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](value)
        prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](solution_dict[parameter])
        improvement_factor_dict[parameter] = 1 / (np.log(prob_no_error) / np.log(baseline_prob_no_error))
    return improvement_factor_dict


def read_param_values(param_names, parameter_file):
    with open(parameter_file, "r") as stream:
        parameter_dict = yaml.load(stream, Loader=SafeLoader)

    return {param_name: parameter_dict[param_name] for param_name in param_names}


if __name__ == "__main__":
    parameters = ["p_loss_init", "coherence_time", "single_qubit_depolar_prob", "ms_depolar_prob",  "emission_fidelity"]
    parser = ArgumentParser()
    parser.add_argument("--solution_param_file", type=str, required=False, default='solution.yaml', help="Name of solution parameter file.")
    parser.add_argument("--baseline_param_file", type=str, required=False, default='baseline.yaml', help="Name of baseline parameter file, defaults to 'baseline.yaml'.")
    parser.add_argument("--parameters", nargs="+", default=parameters, required=False,
                        help=f"Parameters to include in circular plot, defaults to {parameters}.")
    parser.add_argument("--output_path", type=str, required=False, default="circular_plot", help="Path to save plot, defaults to 'cicular_plot'.")
    parser.add_argument("--log", type=str, required=False, default=False, help="Whether or not to use log scale, defaults to False.")

    args, unknown = parser.parse_known_args()

    baseline_param_dict = read_param_values(args.parameters, args.baseline_param_file)
    solution_param_dict = read_param_values(args.parameters, args.solution_param_file)

    imp_factor_dict = compute_improvement_factor_dict(baseline_param_dict, solution_param_dict)

    max_radius = max(imp_factor_dict.values())
    plot_circular(improvement_dictionary=imp_factor_dict,
                  max_radius=max_radius+0.5,
                  baseline_name=args.baseline_param_file.split(".yaml")[0],
                  solution_name=args.solution_param_file.split(".yaml")[0],
                  path=args.output_path,
                  log=args.log)
