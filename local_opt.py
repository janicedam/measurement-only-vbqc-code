import numpy as np
import shutil
import yaml
import csv
from ruamel.yaml import YAML
from argparse import ArgumentParser
from yaml.loader import SafeLoader
import subprocess
import json
import math
from datetime import datetime

"""
This script can be used to perform a local optimization. It takes as input a yaml file with a global optimization 
(the output of the GA optimization from BQC_main with YOTSE), as well as the amount of runs per datapoint (determining the confidence interval)
which is split up in segments of size 'run_amount' to limit the memory usage. The last input parameter is 'reduction' which determines 
the stepsize with which the cost is varied. 
"""

script_path = "/home/jvdam/BQC_iontrap_client/BQC_iontrap.py" # Path to simulation script 
baseline_yaml = "/home/jvdam/BQC_iontrap_client/baseline.yaml" # Path to yaml file containing baseline parameters

class IterationError(Exception):
    def __init__(self, msg="Iterations should not be 0: decrease run_amount"):
        super().__init__(msg)

TO_PROB_NO_ERROR_FUNCTION = {"p_loss_init": lambda x: 1.0 - x,
                             "single_qubit_depolar_prob": lambda x: 1.0 - x,
                             "ms_depolar_prob": lambda x: 1.0 - x,
                             "coherence_time": lambda x: np.exp(-1.0 * (1.0 / x)**2),
                             "emission_fidelity": lambda x: (4 * x - 1) / 3
                             }

TO_PARAMETER_VALUE = {"p_loss_init": lambda x: 1.0 - x,
                             "single_qubit_depolar_prob": lambda x: 1.0 - x,
                             "ms_depolar_prob": lambda x: 1.0 - x,
                             "coherence_time": lambda x: np.sqrt(-1.0 / np.log(x)),
                             "emission_fidelity": lambda x: (3 * x + 1) / 4
                             }

with open(baseline_yaml) as f: #find parameters as stored in the yaml file
    baseline_parameters = yaml.load(f, Loader=SafeLoader)


def compute_parameter_cost(param_name, param_value):
    """Computes cost of single parameter.
    """
    if param_name == "coherence_time":
        return (param_value / baseline_parameters[param_name]) ** 2
    else:
        return 1 / (np.log(TO_PROB_NO_ERROR_FUNCTION[param_name](param_value)) / np.log(TO_PROB_NO_ERROR_FUNCTION[param_name](baseline_parameters[param_name])))


def compute_parameter_dict_cost(parameter_dict):
    """Computes overall hardware cost associated to parameters defined in `parameter_dict`.
    """
    parameter_cost = 0
    for name, value in parameter_dict.items():
        parameter_cost += compute_parameter_cost(name, value)
    return parameter_cost


def reduce_cost(param_name, param_value, reduction):
    """Computes the value `param_name` should take given that its previous value was `param_value` and that its cost
    should be reduced by `reduction`.
    """
    parameter_cost = compute_parameter_cost(param_name, param_value)
    if param_name == "coherence_time":
        new_val = np.sqrt(baseline_parameters[param_name] ** 2 * (parameter_cost - reduction))
    else:
        new_prob_no_error_function = \
            np.exp(
                np.log(
                    TO_PROB_NO_ERROR_FUNCTION[param_name](
                        baseline_parameters[param_name])) / (parameter_cost - reduction))
        new_val = TO_PARAMETER_VALUE[param_name](new_prob_no_error_function)
    return new_val

def increase_cost(param_name, param_value, increase):
    """Computes the value `param_name` should take given that its previous value was `param_value` and that its cost
    should be increased by `increase`.
    """
    parameter_cost = compute_parameter_cost(param_name, param_value)
    if param_name == "coherence_time":
        new_val = np.sqrt(baseline_parameters[param_name] ** 2 * (parameter_cost + increase))
    else:
        new_prob_no_error_function = \
            np.exp(
                np.log(
                    TO_PROB_NO_ERROR_FUNCTION[param_name](
                        baseline_parameters[param_name])) / (parameter_cost + increase))
        new_val = TO_PARAMETER_VALUE[param_name](new_prob_no_error_function)
    return new_val


def update_param_file(parameter, value, param_file):
    """Updates the `value` of `parameter` in `param_file`.
    """
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream=stream, Loader=SafeLoader)
    sim_params[parameter] = float(value)
    with open(param_file, "w") as stream:
        yaml.dump(sim_params, stream)

def find_error_prob(num_runs, run_amount, I, G, mbqc_bases, opt_params):
    file_to_write_to = 'sim_results.txt'
    outcomes = []
    script_path = script_path
    run_amount = run_amount
    iterations = math.floor(num_runs/run_amount)
    if iterations == 0:
        raise IterationError()
    last_bit = num_runs - iterations*run_amount
    command = ["python3", script_path, str(I), json.dumps(G),json.dumps(mbqc_bases), json.dumps(opt_params), str(run_amount)]
    for k in range(iterations):
        try:
            process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate()
            return_code = process.returncode
            if process.returncode == 0:
                meas_outcome = stdout.decode()
                outcomes.append(float(meas_outcome))
            else:
                error_mes = stderr.decode()
                print(f"Error running simulation script:\n {error_mes}")
        except subprocess.CalledProcessError as e:
            print(f"Error running the script: {e}")
        print(f"Iteration {k} of {iterations} complete")
    if last_bit != 0:
        command = ["python3", script_path, str(I), json.dumps(G),json.dumps(mbqc_bases), json.dumps(opt_params), str(last_bit)]
        try:
            process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate()
            return_code = process.returncode
            if process.returncode == 0:
                meas_outcome = stdout.decode()
                outcomes.append(float(meas_outcome))
            else:
                error_mes = stderr.decode()
                print(f"Error running simulation script:\n {error_mes}")
        except subprocess.CalledProcessError as e:
            print(f"Error running the script: {e}")
    avg_outcome = sum(outcomes)/len(outcomes)
    print("errorprob: ", 1.0 - avg_outcome)
    if avg_outcome is not None:
        return 1.0 - avg_outcome
    else:
        print('No valid values found in for finding average outcome')

def params_fulfill_target(param_file, parameters, num_runs, run_amount):
    """Performs simulation and evaluates whether parameters defined in `param_file` achieve `target_rate` on the
    configuration defined in `config_file`.
    """
    opt_params = make_parameter_dict(param_file, parameters)
    I = 5 # Number of nodes in graph state
    G = [[0,1], [1,2], [2,3], [3,4]] # Edges of the graph
    mbqc_bases = [0, np.pi/2, -np.pi, -np.pi/2, 0] # Angles in xy plane of Bloch sphere defining the measurement bases for MBQC - adapted based on measurement outcomes automatically
    error_prob = find_error_prob(num_runs, run_amount, I, G, mbqc_bases, opt_params)
    return True if error_prob <= 0.25 else False


def write_to_file(dictionary, output_file_name):
    """Writes dictionary to csv file, with the keys on the first row and the respective values on the second.

    Parameters
    ----------
    dictionary : dict
        Dictionary to be written.
    output_file_name : str
        File to write.
    """

    with open(output_file_name + '.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(dictionary.keys())
        csv_writer.writerow(dictionary.values())


def create_parameter_file(param_dict, output_file_name, path_to_old_param_file):
    """Creates new parameter file from previous parameter file and a parameter dictionary. Does so by taking the old
    parameter file, reading into a dictionary, overwriting the values of the keys also present in `param_dict` with
    their values in `param_dict` and dumping the result into `output_file_name`.

    Parameters
    ----------
    param_dict : dict
        Dictionary where keys are parameter names and values are parameter values.
    output_file_name : str
        Name of file where to dump new parameter dictionary.
    path_to_old_param_file : str
        Path to old parameter file.

    """
    new_param_file_name = output_file_name + '_params.yaml'
    with open(path_to_old_param_file, "r") as stream:
        old_params = yaml.load(stream, Loader=SafeLoader)
    for k, v in param_dict.items():
        old_params[k] = float(v)
    with open(new_param_file_name, "w") as stream:
        yaml.dump(old_params, stream)


def make_parameter_dict(param_file, parameters):
    """Creates dictionary in which the keys are the names of the parameters for which the cost-reduction procedure will
    be performed and the values are their respective values in the input parameter file.
    """

    param_dict = {}
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream=stream, Loader=SafeLoader)
    for param in parameters:
        param_dict[param] = sim_params[param]
    return param_dict


def evaluate_performance_locally_optimized_solution(param_file, parameters, num_runs, run_amount, safe_to_file):
    """Runs simulation for the post-local-optimization parameter set, processes data and saves it.
    """
    opt_params = make_parameter_dict(param_file, parameters) #make new param file?
    I = 5 # Number of nodes in graph state
    G = [[0,1], [1,2], [2,3], [3,4]] # Edges of the graph
    mbqc_bases = [0, np.pi/2, -np.pi, -np.pi/2, 0] # Angles in xy plane of Bloch sphere defining the measurement bases for MBQC - adapted based on measurement outcomes automatically
    error_prob = find_error_prob(num_runs, run_amount, I, G, mbqc_bases, opt_params)
    error_prob.to_csv(safe_to_file + "_output.csv", index=False)


def perform_local_optimization(param_file, parameters, reduction=1, num_runs=73777, run_amount=5000, safe_to_file=datetime.date):
    """This function can be used to perform a local optimization over a given configuration and set of parameters meant
    to achieve a certain performance target.

    We start with a set of parameters defined in `param_file`. It is assumed that these parameters are close to good enough to
    achieve the error probability bound. We iterate over the hardware parameters, decreasing or increasing
    each of them by a value corresponding to decreasing or increasing their cost by `reduction`. We then run the simulation `num_runs`
    times. We repeat the process untill threshold is hit. The process concludes when this has been done for all parameters.

    Finally, the performance of the resulting reduced-cost hardware parameter set is evaluated and stored under
    `locally_optimized_solution_config_output.csv`.

    Parameters
    ----------
    param_file : str
        Name of parameter file (from global optimization).
    parameters : list
        List of names of parameters whose associated cost should be reduced.
    reduction : float
        Stepsize of cost reduction or increase. 
    num_runs : int
        Number of times simulation should be run per data point.
    run_amount : int
        Number of times simulation is run per round (for memory usage limitations)
    safe_to_file : str
        Name of file to safe outcome to. (defaults to current date)
    """
    shutil.copy(param_file, param_file.split('.yaml')[0] + "_copy.yaml")
    parameter_dict = make_parameter_dict(param_file, parameters)
    initial_fulfill = params_fulfill_target(param_file=param_file, parameters=parameters, num_runs=num_runs, run_amount=run_amount)
    if not initial_fulfill: 
        #Because the global opt is done with lower amount of points, it is possible the given opt does not satisfy bound because of confidence interval.
        #If so, increase cost sligtly until threshold reached (with the same stepsize the cost would otherwise be decreased).
        while True:
            for parameter in parameters:
                print("Varying {}.".format(parameter))
                
                new_param_value = increase_cost(param_name=parameter,
                                            param_value=parameter_dict[parameter],
                                            increase=reduction)
                print("New {} value: {}.".format(parameter, new_param_value))
                update_param_file(parameter=parameter, value=new_param_value, param_file=param_file)
                current_fulfill_check = params_fulfill_target(param_file=param_file, parameters=parameters, num_runs=num_runs, run_amount=run_amount)
                if current_fulfill_check:
                    parameter_dict[parameter] = new_param_value
                    break
                else:
                    parameter_dict[parameter] = new_param_value
            if current_fulfill_check:
                break
    else:    
        while True:
            for parameter in parameters:
                print("Varying {}.".format(parameter))
                
                new_param_value = reduce_cost(param_name=parameter,
                                            param_value=parameter_dict[parameter],
                                            increase=reduction)
                print("New {} value: {}.".format(parameter, new_param_value))
                update_param_file(parameter=parameter, value=new_param_value, param_file=param_file)
                current_fulfill_check = params_fulfill_target(param_file=param_file, parameters=parameters, num_runs=num_runs, run_amount=run_amount)
                if not current_fulfill_check:
                    break
                else:
                    parameter_dict[parameter] = new_param_value
            if not current_fulfill_check:
                break
    parameter_dict["cost"] = compute_parameter_dict_cost(parameter_dict)
    write_to_file(parameter_dict, "locally_optimized_solution")
    create_parameter_file(parameter_dict, "locally_optimized_solution", param_file)
    shutil.copy(param_file.split('.yaml')[0] + "_copy.yaml", param_file)
    evaluate_performance_locally_optimized_solution(param_file='locally_optimized_solution_params.yaml',
                                                    parameters=parameters,
                                                    num_runs=num_runs,
                                                    safe_to_file=safe_to_file)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('param_file', type=str, help="Name of the parameter file (parameters from YOTSE output).")
    parser.add_argument('-n', '--num_runs', required=False, type=int, default=73777,
                        help="Number of runs per configuration. Defaults to 73777.")
    parser.add_argument('--run_amount', required=False, type=int, default=5000,
                        help="Amount of repetitions per run of the protocol (to limit memory usage) defaults to 5000")
    parser.add_argument('--reduction', required=False, type=float, default=1,
                        help="Cost value by which parameters should be made worse on each step. Defaults to 1.")

    args, unknown = parser.parse_known_args()
    parameters = ["p_loss_init", "single_qubit_depolar_prob", "ms_depolar_prob", "coherence_time", "emission_fidelity"]
    perform_local_optimization(param_file=args.param_file,
                               num_runs=args.num_runs,
                               parameters=parameters,
                               reduction=args.reduction,
                               run_amount=args.run_amount)
