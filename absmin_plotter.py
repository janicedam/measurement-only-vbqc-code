import matplotlib.pyplot as plt
import matplotlib
import numpy as np

"""This script is used to make the barplot that displays the absolute minimal requirements (function 'barplotter').
for this, the values to-be-plotted (the abs min requirements and error-interval) need to be manually inserted. 
These values are extracted using the 'finder.py' script. In this script you can also find 'plotter', which is how the
absolute min requirements were found before finder.py was written, and can be used to vizualize this process.
(Though for this you need to manually insert the values for the parameters and their corresponding average outcome (=1-erroprob)
in the lists below, which is not ideal, hence the finder.py script, which is also more accurate.)"""

coherence_time_x = [47500000, 46500000, 45000000, 45500000, 46000000, 44500000, 42500000]
coherence_time_y = [0.7688168399, 0.76274448676, 0.7532428805725362, 0.7564146007563333, 0.7632053349960015, 0.7529040215785407, 0.73700475802757]
coherence_time_yerr = np.ones(len(coherence_time_y))*0.005

emission_fidelity_x = [0.855, 0.845, 0.84, 0.83, 0.8425, 0.8475, 0.85, 0.86]
emission_fidelity_y = [0.7632459980752809, 0.7522534123100695, 0.7422367404475649, 0.7312983721213928, 0.7493256706019491, 0.7551675996584302, 0.7537579462434092, 0.7657671089906068]
emission_fidelity_yerr =  np.ones(len(emission_fidelity_y))*0.005

ms_depolar_prob_x = [0.09, 0.1, 0.11, 0.12, 0.105, 0.107, 0.103, 0.106, 0.108, 0.109, 0.111, 0.112, 0.113]
ms_depolar_prob_y = [0.784539897257953, 0.7658213264296461, 0.7496509752361847, 0.7313525895604321, 0.7584206460007862, 0.7495289859983464, 0.7603724738061998, 0.7528498041395014, 0.7545034360301991, 0.7512774984073627, 0.7452458083142443]
ms_depolar_prob_yerr = np.ones(len(ms_depolar_prob_y))*0.005

single_qubit_depolar_prob_x = [0.01, 0.02, 0.03, 0.021, 0.022, 0.023, 0.024, 0.025, 0.026, 0.027, 0.028, 0.0235]
single_qubit_depolar_prob_y = [0.8741342152703417, 0.7767325860362986, 0.7049758054678287, 0.7705517979858222, 0.76405925966087, 0.7549236211827535, 0.7460726242595931, 0.7389430310259295, 0.732396275261938, 0.7241687788877292, 0.7184488390690865, 0.7507082152974505]
single_qubit_depolar_prob_yerr = np.ones(len(single_qubit_depolar_prob_y))*0.005

def xinterpolator(x1, x2, y1, y2, dy1, dy2):
    #linear interpolator of points (x1, y1 pm dy1); (x2, y2 pm dy2) at y=0.25
    y = 0.25
    x = (y-y1)*(x2-x1)/(y2-y1) + x1 
    xpdx = (y-(y1-dy1))*(x2-x1)/((y2-dy2)-(y1-dy1)) + x1 
    print(f'x: {x}, dx: {xpdx}, dy1: {dy1}, dy2: {dy2}')
    dx = np.abs(xpdx-x)
    return x, dx

def yinterpolator(x1, x2, y1, y2, x):
    #linear interpolator of points (x1, y1 pm dy1); (x2, y2 pm dy2) at y=0.25
    y = (x-(x1))*(y2-y1)/((x2)-(x1)) + y1 
    return y

def find_points_closest_to_threshold(data_list, threshold):
    above_threshold = None
    below_threshold = None
    closest_above_diff = float('inf')
    closest_below_diff = float('inf')

    for y in data_list:
        diff = abs(y - threshold)

        if y > threshold and diff < closest_above_diff:
            above_threshold = y
            closest_above_diff = diff

        if y < threshold and diff < closest_below_diff:
            below_threshold = y
            closest_below_diff = diff

    return above_threshold, below_threshold

def sort_x_and_corresponding_y(x_list, y_list):
    # Combine the x and y coordinates into pairs
    data_pairs = zip(x_list, y_list)

    # Sort the pairs based on the x coordinates
    sorted_data_pairs = sorted(data_pairs, key=lambda pair: pair[0])

    # Extract the sorted x and y coordinates back into separate lists
    sorted_x, sorted_y = zip(*sorted_data_pairs)

    return sorted_x, sorted_y

def find_crossing_points(x, y, threshold):
    crossing_points = []
    for i in range(1, len(x)):
        x0, x1 = x[i - 1], x[i]
        y0, y1 = y[i - 1], y[i]

        if y0 <= threshold <= y1 or y1 <= threshold <= y0:
            # Linear interpolation to find the crossing point
            crossing_x = x0 + (x1 - x0) * (threshold - y0) / (y1 - y0)
            crossing_points.append((crossing_x, threshold))

    return crossing_points

def plotter(variable):
     # Create a list to store the corresponding x and y lists
    x_list = []
    y_list = []
    err_list = []

    # Loop through all variables in the global namespace
    for var_name in globals():
        # Check if the variable name matches the pattern name_x or name_y
        if var_name.startswith(variable + '_'):
            # Extract the suffix (either _x or _y)
            suffix = var_name[len(variable):]

            # Determine whether it is the x or y list and store it accordingly
            if suffix == '_x':
                x_list = globals()[var_name]
            elif suffix == '_y':
                y_list = globals()[var_name]
            elif suffix == '_yerr':
                err_list = globals()[var_name]

    # Check if both x and y lists were found
    if not x_list or not y_list:
        print(f"Error: Could not find matching lists for '{variable}'")
        return

    errorprob = [1-i for i in y_list]
    errtops = [y + dy for y, dy in zip(errorprob, err_list)]
    errbots = [y - dy for y, dy in zip(errorprob, err_list)]

    # Set the backend to display plots in a separate window
    matplotlib.use('TkAgg')

    # Sort data
    sorted_x, sorted_y = sort_x_and_corresponding_y(x_list, errorprob)
    sorted_x, sorted_ytops = sort_x_and_corresponding_y(x_list, errtops)
    sorted_x, sorted_ybots = sort_x_and_corresponding_y(x_list, errbots)

    # Find crossings with threshold line
    crossing_points = find_crossing_points(sorted_x, sorted_y, 0.25)
    crossing_points_tops = find_crossing_points(sorted_x, sorted_ytops, 0.25)
    crossing_points_bots = find_crossing_points(sorted_x, sorted_ybots, 0.25)
    crossingx, crossingy = zip(*crossing_points)
    crossingx_tops, crossingy_tops = zip(*crossing_points_tops)
    crossingx_bots, crossingy_bots = zip(*crossing_points_bots)
    crossingx = list(crossingx)[0]
    crossingx_bots = list(crossingx_bots)[0]
    crossingx_tops = list(crossingx_tops)[0]
    crossingy = list(crossingy)[0]
    crossingy_bots = list(crossingy_bots)[0]
    crossingy_tops = list(crossingy_tops)[0]
        
    # Plot
    plt.errorbar(sorted_x, sorted_y, yerr=err_list, fmt='o', linestyle='solid', label='Simulation outcome')
    plt.plot(sorted_x, sorted_ytops, linestyle='--', color='springgreen')
    plt.plot(sorted_x, sorted_ybots, linestyle='--', color='springgreen')
    plt.xlabel(f'{variable}')
    plt.ylabel('Error probability')
    plt.plot(x_list, np.ones(len(x_list))*0.25, label='Requirement')
    plt.scatter(x=[crossingx, crossingx_bots, crossingx_tops], y=[crossingy, crossingy_bots, crossingy_tops], marker='x',  label='Interpolated x and error', color='red')
    plt.vlines(x=[crossingx_bots, crossingx_tops], ymin = (min(errorprob)-0.005)*np.ones(2), ymax=[0.25, 0.25], colors='red', linestyles='--')
    plt.vlines(x=[crossingx], ymin = (min(errorprob)-0.005)*np.ones(1), ymax=[0.25], colors='red', linestyles='solid')
    plt.legend()
    plt.show()

    print(f"Interpolated value for {variable}: {crossingx} interval {crossingx_tops}-{crossingx_bots}")

#plotter('coherence_time')

def barplotter():
    # Total errorprobability: comparison state-of-the-art (all errors combined) and target
    state_of_the_art_errorprob = 0.506
    required_errorprob = 0.25
    num_runs = 65000 # Repetitions of the simulation script
    confidence_interval_errorprob = np.sqrt(np.log(2/0.05)/(2*num_runs)) # 95% confidence interval according to Hoeffding's bound
    # Define your parameters and their values for 'minimal requirement' and 'state of the art'
    parameters = ['Coherence \n time [ms]', 'Emission \n fidelity', 'Entangling \n gate fidelity', 'Single qubit \n gate fidelity']
    min_requirements = [11.922567, 0.9149010467529296, 1-0.10659793814432988/2, 1-0.021358780008278143/2]
    state_of_the_art = [62.00, 0.974, 0.95, 0.99]
    confidence_intervals = [0.544627, 0.002220528473908967, 0.0027782465457444894/2, 0.0007529209130075418/2]

    # Determine the maximum value among all 'minimal requirement' and 'state of the art' values
    #max_values = [max(x, y) for x, y in zip(min_requirements, state_of_the_art)]
    normalized_min_requirements = [11.922567/62, 1-0.021358780008278143/2, 1-0.10659793814432988/2, 0.9149010467529296]
    normalized_state_of_the_art = [1, 0.99, 0.95, 0.974]
    normalized_confidence_intervals = [0.544627/62, 0.0007529209130075418/2, 0.0027782465457444894/2, 0.002220528473908967]
    min_requirements_errors = np.array(list(zip(normalized_confidence_intervals, normalized_confidence_intervals))).T

    # Normalize the values for each parameter
    #normalized_min_requirements = [min_requirements[n] / max_values[n] for n in range(len(min_requirements))]
    #normalized_state_of_the_art = [state_of_the_art[n] / max_values[n] for n in range(len(state_of_the_art))]
    #normalized_errorprob = required_errorprob/state_of_the_art_errorprob

    # Create a figure and axis
    fig, ax = plt.subplots()
    ax.axis('off')
    bar_width = 0.3

    # Add extra bars for 'overall error probability' and 'error probability threshold' at the bottom
    overall_error_prob = state_of_the_art_errorprob
    threshold_error_prob = required_errorprob
    ax.barh(-1, threshold_error_prob/overall_error_prob, 0.3, label='Threshold', color='#83c995', hatch='/', clip_on=False)
    ax.barh(-1-bar_width, 1, bar_width, label='Overall error', color='#e9d043', clip_on=False, xerr=confidence_interval_errorprob/overall_error_prob, capsize=5)
    ax.text(-0.01, -1-bar_width/2, 'Error \n probability', va='center', color='black', horizontalalignment='right')
    ax.text(threshold_error_prob/overall_error_prob + 0.01, -1-bar_width/4, f'{threshold_error_prob:.3f}')
    ax.text(1+confidence_interval_errorprob/overall_error_prob + 0.01, -1-bar_width-bar_width/4, f'{overall_error_prob:.3f} $\pm$ {confidence_interval_errorprob:.3f}')

    # Create a horizontal bar plot
    index = np.arange(len(parameters))
    bar1 = ax.barh(index + bar_width, normalized_state_of_the_art, bar_width, label='Baseline', hatch='/', color='#7b85d4')
    bar2 = ax.barh(index, normalized_min_requirements, bar_width, label='Minimal requirement', xerr=min_requirements_errors, capsize=5, color='#f37738')
    
    # Remove x-axis
    ax.get_xaxis().set_visible(False)

    # Set the same maximum length for both bars (1.0 in the normalized scale)
    ax.set_xlim(0, 1.2)

    # Add seperation lines
    ax.plot([0, 1], [0.65, 0.65], linestyle='--', color='black', linewidth=1.5, dashes=(5, 3))
    ax.plot([0, 1], [-0.5, -0.5], color='black', linewidth=1.5)

    # Add parameter values as text next to the bars
    for i, (min_norm, soa_norm, minerr) in enumerate(zip(normalized_min_requirements, normalized_state_of_the_art, normalized_confidence_intervals)):
        if parameters[i]=='Single qubit \n gate fidelity': 
            ax.text(min_norm+minerr + 0.01, i, f'{min_requirements[i]:.4f} $\pm$ {confidence_intervals[i]:.4f}', va='center', color='black')
            ax.text(soa_norm + 0.01, i + bar_width, f'{state_of_the_art[i]}', va='center', color='black')
            ax.text(-0.01, i+bar_width/2, parameters[i], va='center', color='black', horizontalalignment='right')
        elif parameters[i]=='Coherence \n time [ms]':
            ax.text(min_norm+minerr + 0.01, i, f'{min_requirements[i]:.2f} $\pm$ {confidence_intervals[i]:.2f}', va='center', color='black')
            ax.text(soa_norm + 0.01, i + bar_width, f'{state_of_the_art[i]}', va='center', color='black')
            ax.text(-0.01, i+bar_width/2, parameters[i], va='center', color='black', horizontalalignment='right')
        else:
            ax.text(min_norm+minerr + 0.01, i, f'{min_requirements[i]:.3f} $\pm$ {confidence_intervals[i]:.3f}', va='center', color='black')
            ax.text(soa_norm + 0.01, i + bar_width, f'{state_of_the_art[i]}', va='center', color='black')
            ax.text(-0.01, i+bar_width/2, parameters[i], va='center', color='black', horizontalalignment='right')
        
    # Add a legend where the two different types are shown side by side
    handles, labels = ax.get_legend_handles_labels()
    print(handles)
    ax.legend([handles[2], handles[3], handles[0], handles[1]], 
              [labels[2], labels[3], labels[0], labels[1]],
              loc='upper center', bbox_to_anchor=(0.5, 1.12), ncol=2)

    # Display the plot
    plt.show()
barplotter()