"""This script resumes an optimiziation in YOTSE that was cut off for some reason 
"""
from examples.wobbly_function.example_wobbly_main import remove_files_after_run
from BQC_main import BQC_pre
from yotse.execution import Executor


def main() -> None:
    """Executes the resuming of an optimization experiment for a 'BQC_measonly'.

    This function resumes the optimization from the last saved state.
    """
    print(" --- Running BQC_measonly resume. --- ")

    # write resume parameter to experiment
    resume_experiment = BQC_pre()
    experiment = Executor(experiment=resume_experiment)
    resume_experiment.system_setup.cmdline_arguments[
        "--resume"
    ] = experiment.aux_dir

    resume_example = Executor(experiment=resume_experiment)
    for i in range(
        resume_example.optimizer.optimization_algorithm.optimization_instance.generations_completed,
        resume_experiment.optimization_information_list[0].parameters[
            "num_generations"
        ],
    ):
        assert (
            resume_example.optimizer.optimization_algorithm.optimization_instance.generations_completed
            == i
        )
        resume_example.run(step_number=i, evolutionary_point_generation=True)

    resumed_solution = resume_example.optimizer.suggest_best_solution()

    print("Resumed solution: ", resumed_solution)


if __name__ == "__main__":
    main()