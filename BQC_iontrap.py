#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  9 10:20:37 2023

@author: janice
"""
import numpy as np
import netsquid as ns
import netsquid.components.instructions as instr
from netsquid.components.qprocessor import PhysicalInstruction
from netsquid.components.models.qerrormodels import DepolarNoiseModel
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.components.component import Message
from netsquid.components import QuantumChannel, ClassicalChannel
from netsquid.nodes.connections import Connection
from netsquid.components.qprogram import QuantumProgram
from netsquid.protocols import NodeProtocol
from netsquid.nodes.network import Network
from netsquid.components.models import FibreDelayModel, FibreLossModel
import random
import sys
#from ion_trap_local import IonTrap
sys.path.insert(0, "/home/janice/netsquid-trappedions/netsquid_trappedions")
from ion_trap import IonTrap
from instructions import IonTrapMSGate
import yaml
from yaml.loader import SafeLoader
#from instructions_local import XX
from argparse import ArgumentParser
import ast  # ast module is used to safely evaluate the string representation of the dictionary

steady_param_yaml = "/home/janice/measurement-only-vqbc/steady_params_perf.yaml" # Path to yaml file containing the paramters that are not varied over

with open(steady_param_yaml) as f: #find parameters as stored in the yaml file
    steady_params = yaml.load(f, Loader=SafeLoader)

def create_ClientProcessor(I):
    darkcount_noise_model = DepolarNoiseModel(depolar_rate=steady_params["darkcount_rate"])
    phys_instrs = [PhysicalInstruction(instr.INSTR_UNITARY, duration=2),
                   PhysicalInstruction(instr.IMeasureFaulty(name='faulty client measurement',
                                                            p0=steady_params['PBS_crosstalk'],
                                                            p1=steady_params['PBS_crosstalk']),
                                        duration=1,
                                       quantum_noise_model=darkcount_noise_model),
                    PhysicalInstruction(instr.INSTR_DISCARD, duration=2)]
    qproc_c = QuantumProcessor("clientqproc", num_positions=1, phys_instructions=phys_instrs, replace=False)
    return qproc_c

def waveplate(retardance, retardation_deviation, angle_fast_axis, fast_axis_tilt):
    """ Jones matrix of a waveplate with specific retardance (pi/2 for QWP, pi for HWP) with errors

    parameters
    ----------
    retardance: float
        relative phase retardance by waveplate (pi/2 for QWP, pi for HWP)
    retardation_deviation: float
        error in retardance
    angle_fast_axis: float
        angle of the fast axis of the waveplate wrt the x axis
    fast_axis_tilt: float
        error in fast axis angle

    Returns
    --------
    :class:'~netsquid.qubits.operators.Operator'
        An operator acting as a wave plate with errors
    """
    deltap = retardance + retardation_deviation
    xp = angle_fast_axis + fast_axis_tilt
    return ns.qubits.operators.Operator("WP", [[(np.exp(deltap*1j/2)*np.cos(xp)**2 + np.exp(-deltap*1j/2)*np.sin(xp)**2), (np.exp(deltap*1j/2)-np.exp(-deltap*1j/2))*np.cos(xp)*np.sin(xp)], 
                                                [(np.exp(deltap*1j/2) - np.exp(-deltap*1j/2))*np.cos(xp)*np.sin(xp), np.exp(deltap*1j/2)*np.sin(xp)**2 + np.exp(-deltap*1j/2)*np.cos(xp)**2]])


def meas_angle_delta(i, colour):
    """ Function to calculate the angle to be send from the Client to the Server in which it will measure the qubits.
    """
    if colour == 1: # Trap qubit: to be measured in preparation basis
        # Generate random bit and keep track of it
        r_i = random.randrange(0, 2)
        r.append(r_i)
        # Get measurement basis and measurement result for rsp of qubit i
        m_i = m[i]
        theta_i = theta[i]
        thetap_i = theta_i + m_i*np.pi
        delta_i = thetap_i + r_i*np.pi
        return delta_i % (2*np.pi)
    else: # Dummy qubit: to be measured in randomly chosen basis
        k = random.randrange(0, 8)
        return k*np.pi/4

def find_first_unfilled_position(positions_data):
    # Helper function to find the lowest onfilled postion in the graph
    for position, data in positions_data.items():
        if not data['filled']:
            return int(position[len("position"):])
    return None  # Return None if all positions are filled

def find_trap_index(colouring, target_position):
        # Helper fucntion to find the index of the trap qubit (counts the number of traps before given position)
        count = 0
        for position in range(target_position):
            if colouring[position] == 1:
                count += 1
        return count

def get_neighbors(index, length):
    """
    Helper function to get index neighbors for a given index in a list.

    Parameters:
    - index: The index for which neighbors are to be found.
    - length: The length of the list.

    Returns:
    - neighbors: A list of neighbor indices.
    """
    neighbors = []

    # Check if there is a neighbor to the left
    if index > 0:
        neighbors.append(index - 1)

    # Check if there is a neighbor to the right
    if index < length - 1:
        neighbors.append(index + 1)

    return neighbors

def check_for_cutoff(positions_data, current_attempt_count, cutoff):
    # Helper function to check if any of the filled memory positions has kept a qubit for longer than 'cutoff' while in attempt 'current_attempt_counter'
    for position, data in positions_data.items():
        filled_value = data.get('filled')

        if filled_value is not None and current_attempt_count - filled_value > cutoff:
            return int(position[len("position"):])
    return None

class PrepareGraph(QuantumProgram):
    """ Prepares graph state from initialized qubits in the server memory by applying CZ to the edges defined
    by G. """
    def program(self, G, I):
        for g in G:
            #Prepare graph state to qubits in the memory: apply CZ gate with native gates
            q1, q2 = g
            self.apply(instr.INSTR_ROT_Y, q1, angle=np.pi/2)
            self.apply(instr.INSTR_ROT_Y, q2, angle=np.pi/2)
            self.apply(instr.INSTR_ROT_X, q2, angle=np.pi)
            self.apply(IonTrapMSGate(num_positions=2), [q1, q2])
            self.apply(instr.INSTR_ROT_Y, q2, angle=-np.pi/2)
            self.apply(instr.INSTR_ROT_X, q1, angle=-np.pi/2)
            self.apply(instr.INSTR_ROT_Y, q1, angle=-np.pi/2)
            self.apply(instr.INSTR_ROT_Z, q2, angle=np.pi/2)
            yield self.run()

class IonTrapEmitProg(QuantumProgram):
    def program(self, memory_position, emission_position):
        self.apply(instr.INSTR_INIT, memory_position)
        self.apply(instr.INSTR_EMIT, [memory_position, emission_position], header="test")
        yield self.run()


class Measure_Client3wp(QuantumProgram):
    """ Program for client to measure incoming photonic qubit in basis
    along the equator of the Bloch sphere using rotations
    by half and quarter wave plates. Saves measurement outcomes in the list m.

    parameter
    ---------
    i: int
    index of the qubit to be measured

    """
    def program(self, i, retardation_deviation, fast_axis_tilt, colour):
        if colour == 1: # Prepare trap qubit
            # Remotely prepare the qubits in |+_theta>, by measuring along a +/-_theta basis
            # Choose a randomly and independenly chosen measurement angle theta
            k = 0 #random.randrange(0, 8)
            theta_i = k*np.pi/4
            #print('thetalist = ', theta, ' i = ', i)
            theta[i] = theta_i # Keep track of the angles used
            # Set fast axes of waveplates corresponding to a measurement in the theta basis
            a = 0
            b = theta_i + np.pi
            c = -np.pi/2
            phi1 = -np.pi/4 + (a + b -c)/4
            phi2 = np.pi/4 + (a+b)/2
            phi3 = np.pi/4 + a/2
            # Apply the 'waveplate' operators (one HWP followed by to QWPs)
            self.apply(instr.INSTR_UNITARY, 0, operator=waveplate(np.pi, retardation_deviation, phi1, fast_axis_tilt))
            self.apply(instr.INSTR_UNITARY, 0, operator=waveplate(np.pi/2, retardation_deviation, phi2, fast_axis_tilt))
            self.apply(instr.INSTR_UNITARY, 0, operator=waveplate(np.pi/2, retardation_deviation, phi3, fast_axis_tilt))
        self.apply(instr.IMeasureFaulty(name='faulty client measurement',
                                                            p0=steady_params['PBS_crosstalk'],
                                                            p1=steady_params['PBS_crosstalk']), 0, output_key=f"m{i}")
        self.apply(instr.INSTR_DISCARD, 0)
        yield self.run()
        m_i =  self.output[f"m{i}"][0]
        m[i] = m_i # Keep track of the measurement outcomes

class Measure_Server(QuantumProgram):
    """ Program for the server to perform the measurements in the +/-_delta basis (as instructed by the client).
    It is possible to adjust this such that the last qubit is measured in the standard basis (or any other basis)
    rather than in a basis along the equator of the Bloch sphere, but this is currently set to False.

    Parameters
    ----------
    i : (int)
        Qubit to be measured
    delta : (float)
        Measurement angle (angle wrt the positive x-axis on the equator of the Bloch sphere) provided by the client

    Returns
    -------
    b : (int)
        Measurement outcome

    """
    def program(self, i, delta, I):
        # lastqubit = False #i==I-1
        # if lastqubit:
        #     # Measure last quof the protocbit in standard basis
        #     self.apply(instr.INSTR_ROT_Z, i, angle=delta)
        #     faulty_measurement_instruction = instr.IMeasureFaulty("faulty_z_measurement_ion_trap",
        #                                                     p0=steady_params["prob_error_0"],
        #                                                     p1=steady_params["prob_error_1"])
        #     self.apply(faulty_measurement_instruction, i, output_key=f"b{i}")
        #     yield self.run()
        #     return self.output[f"b{i}"]
        # else:
        # If not last qubit, measure in +/-_delta
        #self.apply(instr.INSTR_ROT_Z, i, angle=delta+np.pi)
        #self.apply(instr.INSTR_ROT_Y, i, angle=-np.pi/2)
        self.apply(instr.INSTR_ROT_Z, i, angle=delta+np.pi)
        self.apply(instr.INSTR_ROT_Y, i, angle=np.pi/2)
        faulty_measurement_instruction = instr.IMeasureFaulty("faulty_z_measurement_ion_trap",
                                                        p0=steady_params["prob_error_0"],
                                                        p1=steady_params["prob_error_1"])
        self.apply(faulty_measurement_instruction, i, output_key=f"b{i}")
        yield self.run()
        b_i = self.output[f"b{i}"]
        return b_i

class ServerProtocol(NodeProtocol):
    """ Protocol for server to perform a round of rVBQC with the client.
    """
    def __init__(self, node):
        self._deltas = []
        self._qubit_arrived_confirmation = False
        self.opt_params = None
        self.I = None
        self._attempt_counter = 0
        self.positions_data = {'position0': {'filled': None},
                               'position1': {'filled': None},
                               'position2': {'filled': None},
                               'position3': {'filled': None},
                               'position4': {'filled': None},}
        super().__init__(node=node, name="Server_Protocol")

    def start(self, opt_params):
        self.opt_params=opt_params
        super().start()

    def _handle_angle_income(self, msg):
        # When the client sends a measurement angle, we add it to the list of delta's, such that we can measure in this basis later
        delta = msg.items[0]
        self._deltas.append(delta)

    def run(self):
        #Receive graph state description
        description_arrived = yield self.await_port_input(self.node.ports["cin_s"])
        if description_arrived:
            mes = self.node.ports["cin_s"].rx_input()
            self.I = mes.items[0]
            G = mes.items[1]
        self.node.ports["cin_s"].bind_input_handler(self._handle_angle_income)
        graph_prog = PrepareGraph()
        ent_prog = IonTrapEmitProg()
        while sum(1 for data in self.positions_data.values() if data.get('filled') is None) > 0 : #If there are any unfilled positions, keep going
            unused_mem_positions = self.node.subcomponents["ion_trap_quantum_communication_device"].unused_positions
            self.node.subcomponents["ion_trap_quantum_communication_device"].execute_program(ent_prog, memory_position=unused_mem_positions[0], emission_position=self.I) #prepare EPR pair
            yield self.await_program(self.node.subcomponents["ion_trap_quantum_communication_device"]) # Wait for the program to finish running
            yield self.await_timer(1)
            self.node.ports["IDout_s"].tx_output([str(self._attempt_counter), str(unused_mem_positions[0])])
            self._attempt_counter += 1

            # Wait to see if the client received the qubit
            expr = yield self.await_port_input(self.node.ports["IDin_s"])
            if expr.value:
                ID = self.node.ports["IDin_s"].rx_input() 
                attempt_time = 500000 # Time it takes to complete one attempt of sending an ion-entangled photon to the client
                attempts_before_cutoff = int((self.opt_params['coherence_time']/2)/attempt_time) # How many attempts we allow before cutoff (roughly half a coherence time)
                cutted_qubit = check_for_cutoff(self.positions_data, self._attempt_counter, attempts_before_cutoff)
                bool_cutoff = cutted_qubit is not None# If any qubit has been in the memory for too many attempts, bool_cutoff=True
                bool_received = "received "+str(self._attempt_counter-1) in ID.items[0] # Bool to reflect if a qubit was received (client will communicate this clasically)

                if bool_cutoff and bool_received:
                    # If the qubit was received, but there is also another qubit in the memory for too long, we add the new qubit, but throw away the too-old qubit
                    k = cutted_qubit # Which qubit needs to be cutoff
                    self.node.subcomponents["ion_trap_quantum_communication_device"].discard(k) # Discard old qubit
                    self.positions_data['position'+str(unused_mem_positions[0])]['filled'] = self._attempt_counter-1 # Add new, received qubit to the list that keeps track of qubit ages
                    self.positions_data['position'+str(k)]['filled'] = None
                    #print(f"S: heard that creation of qubit {str(unused_mem_positions[0])} was succesful")
                    #print(f"S: discard qubit {k}")
                    self.node.ports["IDout_s"].tx_output(f"DISCARD {k}") # Communicate to client which qubit was discarded

                if bool_cutoff and not bool_received:
                    # Qubit was not received, but another qubit surpassed the cutoff time and needs to be discarded 
                    k = cutted_qubit
                    #print(f"S: discard qubit {k}")
                    self.node.subcomponents["ion_trap_quantum_communication_device"].discard(k)
                    self.positions_data['position'+str(k)]['filled'] = None
                    self.node.ports["IDout_s"].tx_output(f"DISCARD {k}")
                    self.node.subcomponents["ion_trap_quantum_communication_device"].discard(unused_mem_positions[0]) # Discard ion of which photon got lost 

                if not bool_cutoff and not bool_received:
                    # Qubit was not received, none of the qubits in the memory have gone over cutoff
                    self.node.subcomponents["ion_trap_quantum_communication_device"].discard(unused_mem_positions[0])

                if not bool_cutoff and bool_received:
                    # Qubit was received, no qubits have gone over cutoff
                    self.positions_data['position'+str(unused_mem_positions[0])]['filled'] = self._attempt_counter-1 # Add new, received qubit to the list that keeps track of qubit ages
                    #print(f"S: heard that creation of qubit {str(unused_mem_positions[0])} was succesful")

        # All qubits have arrived at the client within cutoff, so start graph forming
        self.node.ports["IDout_s"].tx_output("DONE") # Communicate to the client
        attempts.append(self._attempt_counter)
        self.node.subcomponents["ion_trap_quantum_communication_device"].execute_program(graph_prog, G=G, I=self.I) # Make remaining qubits in memory into a graph state
        for j in range(self.I):
            angle_arrived = yield self.await_port_input(self.node.ports["cin_s"]) # Receive measurement angle delta from client
            if angle_arrived:
                if self.node.subcomponents["ion_trap_quantum_communication_device"].busy:
                    yield self.await_program(self.node.subcomponents["ion_trap_quantum_communication_device"]) # Wait for a bit if the server is still busy
                delta_j = self._deltas[j]
                meas_prog = Measure_Server()
                self.node.subcomponents["ion_trap_quantum_communication_device"].execute_program(meas_prog, i=j, delta=delta_j, I=self.I) # Measure the qubit
                yield self.await_program(self.node.subcomponents["ion_trap_quantum_communication_device"])
                b_j = meas_prog.output[f"b{j}"]
                self.node.ports["cout_s"].tx_output(Message(items=b_j, header=f"b{j}")) # Send measurement outcome back to the client

class ClientProtocol(NodeProtocol):
    """ Protocol for client to perform a round of rVBQC on the server.
    """
    def __init__(self, node):
        self.positions_data = {'position0': {'filled': False, 'colour': 0},
                               'position1': {'filled': False, 'colour': 1},
                               'position2': {'filled': False, 'colour': 0},
                               'position3': {'filled': False, 'colour': 1},
                               'position4': {'filled': False, 'colour': 0},}
        self.qindex = 0 # To keep track of which node in the graph is being remotely prepared
        #self.meas_index = 0 # To keep track of which qubit is being measured by the server
        self._rsp_complete = False
        super().__init__(node=node, name=f"ClientProtocol running on node {node}")

    def start(self):
        super().start()

    def _handle_meas_income(self, msg):
        # Decode and safe the measurment outcome that the Server sends back
        # colour = self.positions_data['position'+str(self.meas_index)]['colour']
        # if colour == 1: # Trap
        #     trap_index = find_trap_index(self.positions_data, 'position'+str(self.meas_index))
        #     b = msg.items[0]
        #     r_i = r[trap_index]
        #     s_i = (b + r_i) % 2
        #     s.append(s_i)
        b_i = msg.items[0]
        b.append(b_i)

    def _handle_ID_income(self, msg):
        meas_prog = Measure_Client3wp()
        ID = msg.items
        if "DISCARD" in ID[0]: # Server tells client that a qubit has reached the cutoff, remove measurement
            discarded_qubit = int(ID[0].replace("DISCARD ", ""))
            #print("C: discarded qubit ", discarded_qubit)
            self.positions_data['position'+str(discarded_qubit)]['filled'] = False
            self.qindex = find_first_unfilled_position(self.positions_data)
            m[discarded_qubit] = -1
            if self.positions_data['position'+str(discarded_qubit)]['colour'] == 1:
                theta[discarded_qubit] = -1

        elif "DONE" in ID: # Server tells client that the RSP phase of the protocol is complete
            self._rsp_complete = True

        else: # If not done or discard, a qbitID came in, check if a qubit came into the memory
            if self.node.subcomponents["clientqproc"].used_positions: # There is a qubit: start measurement program and declare success
                self.positions_data['position'+str(self.qindex)]['filled'] = True
                retardation_deviation = steady_params['retardation_deviation']
                fast_axis_tilt=steady_params['fast_axis_tilt']
                self.node.subcomponents["clientqproc"].execute_program(meas_prog, i=self.qindex, retardation_deviation=retardation_deviation, fast_axis_tilt=fast_axis_tilt, colour=self.positions_data['position'+str(self.qindex)]['colour'])
                self.node.ports["IDout_c"].tx_output("received "+str(ID[0]))
                if find_first_unfilled_position(self.positions_data) is not None:
                    self.qindex = find_first_unfilled_position(self.positions_data)
            else: # Photon got lost, communicate this to server
                self.node.ports["IDout_c"].tx_output("did not receive "+str(ID[0]))

    def run(self):
        # Send graph state description to Server (number of qubits and list of edges)
        I = 5 # Number of nodes in the graph 
        G = [[0,1], [1,2], [2,3], [3,4]] # Edges of the graph (between nodes [x,y])
        message = [I, G]
        self.node.ports["cout_c"].tx_output(message)
        self.node.ports["cin_c"].bind_input_handler(self._handle_meas_income)
        self.node.ports["IDin_c"].bind_input_handler(self._handle_ID_income)
        self.node.ports["qin_c"].forward_input(self.node.subcomponents["clientqproc"].ports["qin0"])
        # Choose graph colouring to determine which qubits will serve as trap, and which will serve as dummy
        col = random.randint(0,1)
        if col==1: # Is initiallized in 'colour 0', so only change if colour == 1
            colouring = [1,0,1,0,1]
            for data in self.positions_data.values():
                data['colour'] = (data['colour'] + 1) % 2 # Flip all bit values
        else:
            colouring = [0,1,0,1,0]

        while not self._rsp_complete: # Things are handeled by input handlers untill RSP is complete
            assert not self._rsp_complete
            yield self.await_port_input(self.node.ports["IDin_c"])
        for j in range(I): # For all qubits:
            # Find correct measurement angle to send
            delta_j = meas_angle_delta(i=j, colour=self.positions_data['position'+str(j)]['colour'])
            # Send measurement angle to Server
            mes = Message(items=delta_j, header=f"delta{j}")
            self.node.ports["cout_c"].tx_output(mes)
            # Wait till measruement outcome have been send back (income will be handled by _handle_meas_income)
            yield self.await_port_input(self.node.ports["cin_c"])
        runtimes.append(ns.sim_time())
        # Evaluate test outcomes:
        for i in range(5):
            if colouring[i] == 1: # Trap, so check if condition is met
                trap_index = find_trap_index(colouring, i)
                neighbor_indexes = get_neighbors(i, length=I)
                m_neighbors = [m[n_index] for n_index in neighbor_indexes]
                if b[i] == (r[trap_index] + sum(m_neighbors)) % 2:
                    #print("Trap passed")
                    trap_outcome.append(0) # Trap passed (condition is met)
                else:
                    #print(f"Trap failed")
                    trap_outcome.append(1) # Trap failed (condition is not met)


def networksetupproc(opt_params):
    """
    Creates setup with a trapped-ion quantum server and a photonic client, with one quantum channel and two classical channels
    (one classical channel is just used for qubit IDs, this was just easier in coding).
    Parameters are taken from yaml files (baseline.yaml and steady_params.yaml)
    """
    network = Network("BQC network")
    I = 5 # Number of nodes in the graph

    #Create nodes and add to network
    server = ns.nodes.node.Node("server", port_names=["qout_s", "cin_s", "cout_s", "IDout_s", "IDin_s"])
    client = ns.nodes.node.Node("client", port_names=["qin_c", "cout_c", "cin_c", "IDout_c", "IDin_c"])
    network.add_nodes([server, client])

    #Add quantum processor to nodes
    #print("test ", params["coherence_time"])
    qproc_s = IonTrap(num_positions=I, coherence_time=opt_params["coherence_time"],
                      prob_error_0=steady_params["prob_error_0"], prob_error_1=steady_params["prob_error_1"],
                      init_depolar_prob=steady_params["init_depolar_prob"], rot_x_depolar_prob=opt_params["single_qubit_depolar_prob"],
                      rot_y_depolar_prob=opt_params["single_qubit_depolar_prob"], rot_z_depolar_prob=opt_params["single_qubit_depolar_prob"],
                      ms_depolar_prob=opt_params["ms_depolar_prob"], emission_fidelity=opt_params["emission_fidelity"],
                      collection_efficiency=steady_params["collection_efficiency"], emission_duration=steady_params["emission_duration"],
                      measurement_duration=steady_params["measurement_duration"], initialization_duration=steady_params["initialization_duration"],
                      single_qubit_rotation_duration=steady_params["single_qubit_rotation_duration"], ms_pi_over_2_duration=steady_params["ms_pi_over_2_duration"],
                      ms_optimization_angle=steady_params["ms_optimization_angle"])
    server.add_subcomponent(qproc_s)
    qproc_c = create_ClientProcessor(I)
    client.add_subcomponent(qproc_c)

    fibre_length = steady_params['channel_length']
    loss_length = steady_params['p_loss_length']
    #Add quantum connection with quantum channel & connect the ports
    qcon = Connection("Qconnection")
    network.add_connection(server, client, connection=qcon, label="Qconnection", port_name_node1="qout_s", 
                           port_name_node2="qin_c")
    loss_model = FibreLossModel(p_loss_init=opt_params['p_loss_init'], p_loss_length=loss_length)
    qchan = QuantumChannel("qchan", length=fibre_length, models={"delay_model": FibreDelayModel(), 'quantum_loss_model': loss_model})
    server.subcomponents['ion_trap_quantum_communication_device'].ports["qout"].forward_output(server.ports["qout_s"])
    qcon.add_subcomponent(qchan, forward_input=[("A", "send")], forward_output=[("B", "recv")])

    ccon1 = Connection("Cconnection1")
    network.add_connection(client, server, connection=ccon1, label="Connection1", port_name_node1="cout_c", 
                           port_name_node2="cin_s")
    cchan1 = ClassicalChannel("cchan1", length=fibre_length, models={"delay_model": FibreDelayModel()})
    ccon1.add_subcomponent(cchan1, forward_input=[("A", "send")], forward_output=[("B", "recv")])

    ccon2 = Connection("Cconnection2")
    network.add_connection(server, client, connection=ccon2, label="Connection2", port_name_node1="cout_s", 
                           port_name_node2="cin_c")
    cchan2 = ClassicalChannel("cchan2", length=fibre_length, models={"delay_model": FibreDelayModel()})
    ccon2.add_subcomponent(cchan2, forward_input=[("A", "send")], forward_output=[("B", "recv")])

    IDconsc = Connection("IDConnectionsc")
    network.add_connection(server, client, connection=IDconsc, label="IDConnectionsc", port_name_node1="IDout_s", 
                           port_name_node2="IDin_c")
    IDchansc = ClassicalChannel("IDchansc", length=fibre_length, models={"delay_model": FibreDelayModel()})
    IDconsc.add_subcomponent(IDchansc, forward_input=[("A", "send")], forward_output=[("B", "recv")])

    IDconcs = Connection("IDConnectionsc")
    network.add_connection(client, server, connection=IDconcs, label="IDConnectioncs", port_name_node1="IDout_c", 
                           port_name_node2="IDin_s")
    IDchancs = ClassicalChannel("IDchancs", length=fibre_length, models={"delay_model": FibreDelayModel()})
    IDconcs.add_subcomponent(IDchancs, forward_input=[("A", "send")], forward_output=[("B", "recv")])

    return network

# Keep track of...
m = [-1, -1, -1, -1, -1] # Measurement results of RSP measurements by Client
theta = [-1, -1, -1, -1, -1] # Measurement angles used by the client for RSP
r = [] # Random bit used to one-time-pad the angles that the Client sends to the Server for computation
b = [] # Measurement outcomes as send by the Server
attempts = []
trap_outcome = []
runtimes = []
def run_experiment(opt_params, run_amount):
    test_passed = [] # 0 for failed test round, 1 for succesful test round
    for i in range(run_amount):
        # Clear everything 
        ns.sim_reset()
        m = [-1, -1, -1, -1, -1]
        theta = [-1, -1, -1, -1, -1]
        trap_outcome.clear()
        runtimes.clear()
        r.clear()
        b.clear()
        attempts.clear()
        # Set up the network and processors
        network = networksetupproc(opt_params=opt_params)
        server = network.get_node("server")
        client = network.get_node("client")
        server.subcomponents["ion_trap_quantum_communication_device"].reset()
        client.subcomponents["clientqproc"].reset()
        # Start protocols
        ClientProtocol(client).start()
        ServerProtocol(server).start(opt_params=opt_params)
        ns.sim_run()
        if sum(trap_outcome)!=0:
            test_passed.append(1) # One of the traps came back negative, so test round failed
            #print("test round failed")
        else:
            test_passed.append(0) # All traps came back correct, so test round passed
            #print("test round passed")
    result = sum(test_passed)/len(test_passed) # Percentage of failed test rounds
    avg_runtime = sum(runtimes)/len(runtimes)
    print(f"{result},{avg_runtime}")
    return result, avg_runtime

def main():
    # Parse the input argument
    parser = ArgumentParser()
    parser.add_argument('--opt_params', type=str, help="Input dictionary as a string", required=False)
    parser.add_argument('--run_amount', type=int, help="Number of iterations the simulation is repeater for (=number of test rounds)", required=True)
    args = parser.parse_args()
    if args.opt_params:
        try:
            # Safely evaluate the string representation of the dictionary
            args.opt_params = ast.literal_eval(args.opt_params)
        except ValueError:
            print("Error: Unable to parse the input dictionary.")
            exit(1)
    else:
        print('No input dict was provided, so take baseline as default')
        baseline_yaml = "/home/janice/measurement-only-vqbc/baseline_perf.yaml"
        with open(baseline_yaml) as f:
            default_dict = yaml.load(f, Loader=SafeLoader)
        args.opt_params = default_dict
    run_experiment(opt_params=args.opt_params, run_amount=args.run_amount)

if __name__ == "__main__":
    import logging
    ns.logger.setLevel(logging.DEBUG)
    main()




