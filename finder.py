import numpy as np
import yaml
from yaml.loader import SafeLoader
import csv
import os
import subprocess
import math
import json
from argparse import ArgumentParser
from costfunction import find_error_prob

"""This code can be used to find the best estimate on absolute minimal requirements.
This can be used for a global search (using lower num_runs) and more focussed search (using higher num_runs).
All other parameters should be perfect (as in baseline_perf.yaml), we manually change settings for decoherence:
when 'finding' the coherence time best estimate, the decoherence should be turned on, but for all other paramters,
there should be no decoherence, which can be done by commenting out the mem_noise_models in line 152 of ion_trap_local.py.
The scipts calculates the error probability (by running simulation script) and confidence interval (using Hoeffding's bound)
for the input parameters, then continues to half the distance to their mean (difference between value of paramter for point above and below threshold)
untill their confidence intervalls hit the the threshold of 25% error probability. Then a linear interpolation of these closest points is made 
to find the best estimate. 

Parsed parameters
-----------------
parameter : (str)
    Name of the parameter to find absolute minimum of (should be one of the parameters in the baseline)
lower_est : (float)
    Edge value of the paramter that gives a lower error probability (i.e. this value is closer to perfect, e.g. higher coherence time)
    Must have an error probability (including confidence interval) BELOW 0.25
upper_est : (float)
    Edge value of the paramter that gives a higher error probability (i.e. this value is further from perfect, e.g. lower coherence time)
    Must have an error probability (including confidence interval) ABOVE 0.25
filename : (str)
    Name of the file where the outcome will be stored in CSV format 

Returns 
---------------------
Best estimate for absolute minimum and corresponding confidence interval (with 95% confidence level)

"""

class IterationError(Exception):
    def __init__(self, msg="Iterations should not be 0: decrease run_amount"):
        super().__init__(msg)

class NoOutputError(Exception):
    def __init__(self, msg="Simulation script gave no outcome"):
        super().__init__(msg)

class SmallIntervalError(Exception):
    def __init__(self, msg="Confidence interval of edge already hitting bound: choose different edges or increase num_runs."):
        super().__init__(msg)

def save_to_csv(errorprob, filename, parameter_value):
    print("Writing to csv")
    #file_name = f"{parameter}_finder.csv"
    file_path = filename #os.path.join("output", file_name)
    with open(file_path, mode='a', newline='') as file:
        # Create a CSV writer
        writer = csv.writer(file)
        # Write the values to the CSV file
        writer.writerow([parameter_value, errorprob])

# def low_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount, script_path):
#     mid = upper_edge - lower_edge
#     lower_edge = (mid - lower_edge)/2
#     baseline[parameter] = lower_edge
#     errorprob_low = find_error_prob(num_runs, run_amount, baseline, script_path)
#     save_to_csv(errorprob_low, parameter, lower_edge)
#     return errorprob_low

# def up_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount, script_path):
#     mid = upper_edge - lower_edge
#     upper_edge = (upper_edge - mid)/2
#     baseline[parameter] = upper_edge
#     errorprob_up = find_error_prob(num_runs, run_amount, baseline, script_path)
#     save_to_csv(errorprob_up, parameter, upper_edge)
#     return errorprob_up

def xinterpolator(x1, x2, y1, y2, dy1, dy2):
    #linear interpolator of points (x1, y1 pm dy1); (x2, y2 pm dy2) at y=0.25
    y = 0.25
    x = (y-y1)*(x2-x1)/(y2-y1) + x1 
    xpdx_low = (y-(y1-dy1))*(x2-x1)/((y2-dy2)-(y1-dy1)) + x1 
    xpdk_up = (y-(y1+dy1))*(x2-x1)/((y2+dy2)-(y1+dy1)) + x1 
    dx_up = np.abs(xpdk_up-x)
    dx_low = np.abs(x - xpdx_low)
    return x, dx_up, dx_low

def extrapolate_best_estimate(parameter, estimate_low, estimate_up, filename, num_runs, run_amount):
    x_low, y_low, x_up, y_up, confidence = finder(parameter, estimate_low, estimate_up, num_runs, run_amount, filename)
    best_est_x, best_est_dxu, best_est_dxl = xinterpolator(x_low, x_up, y_low, y_up, confidence, confidence)
    print(f"best estimate for {parameter} is {best_est_x} on interval {best_est_dxl}-{best_est_dxu}")
    #file_name = filename
    file_path = filename #os.path.join("output", file_name)
    print('Writing to cvs at ', file_path)
    with open(file_path, mode='a+', newline='') as file:
        # Create a CSV writer
        writer = csv.writer(file)
        # Write the values to the CSV file
        writer.writerow([parameter, best_est_x, best_est_dxl, best_est_dxu])
    return best_est_x, best_est_dxl, best_est_dxu


def finder(parameter, lower_edge, upper_edge, num_runs, run_amount, filename):
    with open('baseline_perf.yaml') as f: #find parameters as stored in the yaml file
        baseline = yaml.load(f, Loader=SafeLoader)

    hitting_bound_up = False
    hitting_bound_low = False

    alpha = 0.05 #confidence level
    confidence_interval = np.sqrt(np.log(2/alpha)/(2*num_runs))
    print(f"Confident interval for num_runs={num_runs} is {confidence_interval}")

    baseline[parameter] = lower_edge
    script_path = "/home/janicedam/measurement-only-vqbc/BQC_iontrap.py"
    errorprob_low, time = find_error_prob(num_runs, run_amount, baseline, script_path)
    save_to_csv(errorprob_low, filename, lower_edge)
    baseline[parameter] = upper_edge
    errorprob_up, time = find_error_prob(num_runs, run_amount, baseline, script_path)
    save_to_csv(errorprob_up, filename, upper_edge)
    print(f'Errorprob lower bound: {errorprob_low} \pm {confidence_interval}; errorprob upper bound: {errorprob_up} \pm {confidence_interval}.')
    def low_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount):
        dif = upper_edge - lower_edge
        lower_edge = lower_edge + dif/4 # New point is halfway towards the midpoint of the upper and lower edges
        baseline[parameter] = lower_edge
        print(f"Setting {parameter} to {lower_edge} in lowstepper")
        errorprob_low, time = find_error_prob(num_runs, run_amount, baseline, script_path)
        print(f"Errorprob outcome lowstepper: {errorprob_low}")
        save_to_csv(errorprob_low, filename, lower_edge)
        return errorprob_low, lower_edge

    def up_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount):
        dif = upper_edge - lower_edge
        upper_edge = upper_edge - dif/4 # New point is halfway towards the midpoint of the upper and lower edges
        baseline[parameter] = upper_edge
        print(f"Setting {parameter} to {upper_edge} in upstepper")
        errorprob_up, time = find_error_prob(num_runs, run_amount, baseline, script_path)
        print(f"Errorprob outcome upstepper: {errorprob_up}")
        save_to_csv(errorprob_up, filename, upper_edge)
        return errorprob_up, upper_edge

    if errorprob_low+confidence_interval > 0.25 or errorprob_up-confidence_interval < 0.25:
        raise SmallIntervalError
    print("point check: no smallintervalerror, bools for hitting bound below and up: ", hitting_bound_low, hitting_bound_up, not hitting_bound_up and hitting_bound_low)
    while not (hitting_bound_up or hitting_bound_low):
        # Decrease difference to midpoint
        print('Decreasing distance to midpoint')
        errorprob_low, lower_edge = low_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount)
        errorprob_up, upper_edge = up_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount)
        if errorprob_low + confidence_interval > 0.25:
            final_errorprob_low = errorprob_low
            final_lower = lower_edge
            print(f'Found final lower bound for {parameter} to be {final_lower} with errorprob {final_errorprob_low} +- {confidence_interval}')
            hitting_bound_low = True
        if errorprob_up - confidence_interval < 0.25:
            final_errorprob_up = errorprob_up
            final_upper = upper_edge
            print(f'Found final upper bound for {parameter} to be {final_upper} with errorprob {final_errorprob_up} +- {confidence_interval}')
            hitting_bound_up = True

    if hitting_bound_low:
        print("Errorprob hit from below")
        while not hitting_bound_up:
            print("Decreasing distance to errorprob from above")
            errorprob_up, upper_edge = up_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount)

            if errorprob_up - confidence_interval < 0.25:
                final_errorprob_up = errorprob_up
                final_upper = upper_edge
                print(f'Found final lower bound for {parameter} to be {final_upper} with errorprob {final_errorprob_up} +- {confidence_interval}')
                hitting_bound_up = True

    if hitting_bound_up:
        print("Errorprob hit from above")
        while not hitting_bound_low:
            print("Decreasing distance to errorprob from below")
            errorprob_low, errorprob_low = low_stepper(parameter, upper_edge, lower_edge, baseline, num_runs, run_amount)

            if errorprob_low + confidence_interval > 0.25:
                final_errorprob_low = errorprob_low
                final_lower = lower_edge
                print(f'Found final lower bound for {parameter} to be {final_lower} with errorprob {final_errorprob_low} +- {confidence_interval}')
                hitting_bound_low = True

    return final_lower, final_errorprob_low, final_upper, final_errorprob_up, confidence_interval

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--parameter', type=str)
    parser.add_argument('--num_runs', type=int, default=102469)
    parser.add_argument('--run_amount', type=int, default=5000)
    parser.add_argument('--lower_est', type=float)
    parser.add_argument('--upper_est', type=float)
    parser.add_argument('--filename', type=str, default="output.csv")
    args = parser.parse_args()
    if args.parameter == "coherence_time":
        print("Make sure decoherence is _on_ in ion_trap_local.py")
    else: print("Make sure decoherence if _off_ in ion_trap_local.py")


    extrapolate_best_estimate(args.parameter, args.lower_est, args.upper_est, args.filename, args.num_runs, args.run_amount)
    
